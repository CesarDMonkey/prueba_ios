//
//  JsonBase.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import Foundation

public class JsonBase {
	public var results : Array<Movie>?
	public var page : Int?
	public var total_results : Int?
	//public var dates : Dates?
	public var total_pages : Int?

    public class func modelsFromDictionaryArray(array:NSArray) -> [JsonBase]
    {
        var models:[JsonBase] = []
        for item in array
        {
            models.append(JsonBase(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

        if (dictionary["results"] != nil) { results = Movie.modelsFromDictionaryArray(array: dictionary["results"] as! NSArray) }
		page = dictionary["page"] as? Int
		total_results = dictionary["total_results"] as? Int
		//if (dictionary["dates"] != nil) { dates = Dates(dictionary: dictionary["dates"] as! NSDictionary) }
		total_pages = dictionary["total_pages"] as? Int
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.page, forKey: "page")
		dictionary.setValue(self.total_results, forKey: "total_results")
		//dictionary.setValue(self.dates?.dictionaryRepresentation(), forKey: "dates")
		dictionary.setValue(self.total_pages, forKey: "total_pages")

		return dictionary
	}

}
