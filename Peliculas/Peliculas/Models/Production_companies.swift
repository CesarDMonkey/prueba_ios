//
//  Production_companies.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import Foundation

public class Production_companies {
	public var id : Int?
	public var logo_path : String?
	public var name : String?
	public var origin_country : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [Production_companies]
    {
        var models:[Production_companies] = []
        for item in array
        {
            models.append(Production_companies(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		logo_path = dictionary["logo_path"] as? String
		name = dictionary["name"] as? String
		origin_country = dictionary["origin_country"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.logo_path, forKey: "logo_path")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.origin_country, forKey: "origin_country")

		return dictionary
	}

}
