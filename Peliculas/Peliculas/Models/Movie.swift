//
//  Movie.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

public class Movie {
    public var popularity : Double?
    public var vote_count : Int?
    public var video : Bool?
    public var poster_path : String?
    public var id : Int?
    public var adult : Bool?
    public var backdrop_path : String?
    public var original_language : String?
    public var original_title : String?
    //public var genre_ids : Array<Int>? No se utiliza
    public var title : String?
    public var vote_average : Double?
    public var overview : String?
    public var release_date : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Movie]
    {
        var models:[Movie] = []
        for item in array
        {
            models.append(Movie(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        
        popularity = dictionary["popularity"] as? Double
        vote_count = dictionary["vote_count"] as? Int
        video = dictionary["video"] as? Bool
        poster_path = dictionary["poster_path"] as? String
        id = dictionary["id"] as? Int
        adult = dictionary["adult"] as? Bool
        backdrop_path = dictionary["backdrop_path"] as? String
        original_language = dictionary["original_language"] as? String
        original_title = dictionary["original_title"] as? String
        //if (dictionary["genre_ids"] != nil) { genre_ids = Genre_ids.modelsFromDictionaryArray(dictionary["genre_ids"] as! NSArray) }
        title = dictionary["title"] as? String
        vote_average = dictionary["vote_average"] as? Double
        overview = dictionary["overview"] as? String
        release_date = dictionary["release_date"] as? String
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.popularity, forKey: "popularity")
        dictionary.setValue(self.vote_count, forKey: "vote_count")
        dictionary.setValue(self.video, forKey: "video")
        dictionary.setValue(self.poster_path, forKey: "poster_path")
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.adult, forKey: "adult")
        dictionary.setValue(self.backdrop_path, forKey: "backdrop_path")
        dictionary.setValue(self.original_language, forKey: "original_language")
        dictionary.setValue(self.original_title, forKey: "original_title")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.vote_average, forKey: "vote_average")
        dictionary.setValue(self.overview, forKey: "overview")
        dictionary.setValue(self.release_date, forKey: "release_date")
        
        return dictionary
    }
    
}

extension Movie: Equatable {
    public static func == (lhs: Movie, rhs: Movie) -> Bool {
        // Two Photos are the same if they have the same movieID
        return lhs.id == rhs.id
    }
}
