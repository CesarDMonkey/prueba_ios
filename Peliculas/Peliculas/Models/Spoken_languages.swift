//
//  Spoken_languages.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import Foundation

public class Spoken_languages {
	public var iso_639_1 : String?
	public var name : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Spoken_languages]
    {
        var models:[Spoken_languages] = []
        for item in array
        {
            models.append(Spoken_languages(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		iso_639_1 = dictionary["iso_639_1"] as? String
		name = dictionary["name"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.iso_639_1, forKey: "iso_639_1")
		dictionary.setValue(self.name, forKey: "name")

		return dictionary
	}

}
