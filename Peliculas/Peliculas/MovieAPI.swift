//
//  MovieViewController.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import Foundation


enum MovError: Error {
    case invalidJSONData
}

private let baseURLString = "https://api.themoviedb.org/3/movie/now_playing"
private let apiKey = "634b49e294bd1ff87914e7b9d014daed"

struct MovieAPI {
    
    static var interestingMoviesURL: URL {
        return flickrURL(parameters: ["extras": ""])
    }
    
    private static func flickrURL(
        parameters: [String:String]?) -> URL {
            var components = URLComponents(string: baseURLString)!
            
            var queryItems = [URLQueryItem]()
            
            let baseParams = [
                "api_key": apiKey,
                "language": "en-US",
                "page": "1",
                ] as [String : Any]
            
            for (key, value) in baseParams {
                let item = URLQueryItem(name: key, value: value as? String)
                queryItems.append(item)
            }
        
            /*if let additionalParams = parameters {
                for (key, value) in additionalParams {
                    let item = URLQueryItem(name: key, value: value)
                    queryItems.append(item)
                }
            }*/
            components.queryItems = queryItems
            return components.url!
    }
    
    static func movies(fromJSON data: Data) -> MoviesResult {
        
        do {
            let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
            
            
            guard
                let json = JsonBase(dictionary: someDictionaryFromJSON as NSDictionary) else{
                    return .failure(MovError.invalidJSONData)
            }
            
            let finalMovies = json.results
            if finalMovies?.isEmpty ?? false{
                return .failure(MovError.invalidJSONData)
            }
            return .success(finalMovies ?? [])
        } catch let error {
            return .failure(error)
        }
    }
    static func movieInfo(fromJSON data: Data) -> MovieInfoResult {
        
        do {
            let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
            
            
            guard
                let json = MovieInfo(dictionary: someDictionaryFromJSON as NSDictionary) else{                
                    return .failure(MovError.invalidJSONData)
            }
            
            return .success(json)
        } catch let error {
            return .failure(error)
        }
    }
}
