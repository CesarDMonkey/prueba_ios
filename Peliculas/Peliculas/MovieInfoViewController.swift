//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class MovieInfoViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var txtTitle: UILabel!
    @IBOutlet var txtDuracion: UILabel!
    @IBOutlet var txtFecha: UILabel!
    @IBOutlet var txtCalificacion: UILabel!
    @IBOutlet var txtGenero: UILabel!
    @IBOutlet var txtDescripcion: UILabel!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    var movie: Movie! {
        didSet {
            navigationItem.title = movie.title
        }
        willSet{
            
        }
    }
    var store: MovieStore!
    private let inputFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY/MM/dd"
        return formatter
    }()
    private let outputFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMMM-YYYY"
        formatter.dateStyle = .long
        //formatter.timeStyle = .medium
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        spinner.assignColor(.blue)
        store.fetchMovieInfo(movieID: movie.id!, completion:{(movieInfo) -> Void in
            switch movieInfo {
                case let .success(info):
                    print("Successfully found \(info) movies.")
                    let date = self.inputFormatter.date(from: info.release_date!)
                    let strFecha = self.outputFormatter.string(from: date!)
                    self.txtTitle.text = info.title
                    self.txtDuracion.text = String(info.runtime!)+" min"
                    self.txtFecha.text = strFecha
                    self.txtCalificacion.text = String(info.vote_average!)
                    self.txtGenero.text = self.procesaGeneros(generos: info.genres!)
                    self.txtDescripcion.text = info.overview
                case let .failure(error):
                    print("Error fetching recent movies: \(error)")
                
            }
           
        })
        store.fetchImage(for: movie,detail: true, completion: { (result) -> Void in
            switch result {
            case let .success(image):
                self.imageView.image = image
                self.spinner.stopAnimating()
            case let .failure(error):
                print("Error fetching image for movie: \(error)")
            }
        })
    }
    func procesaGeneros(generos: Array<Genres>) -> String{
        let stringGeneros = generos.compactMap{ $0.name}
        return stringGeneros.joined(separator: ", ")
    }
}

