//
//  MovieViewController.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class MovieDataSource: NSObject, UICollectionViewDataSource {
    
    var movies: [Movie] = []
    
    func collectionView(_ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int {
            return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let identifier = "MovieCollectionViewCell"
            let cell =
            collectionView.dequeueReusableCell(withReuseIdentifier: identifier,
                for: indexPath) as! MovieCollectionViewCell
            cell.txtTitle.text = movies[indexPath.row].title
            cell.txtCalificacion.text = String(movies[indexPath.row].vote_average!)
            cell.txtFecha.text = movies[indexPath.row].release_date
            return cell
    }
}
