//
//  MovieStore.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

enum ImageResult {
    case success(UIImage)
    case failure(Error)
}

enum MovieError: Error {
    case imageCreationError
}

enum MoviesResult {
    case success([Movie])
    case failure(Error)
}
enum MovieInfoResult {
    case success(MovieInfo)
    case failure(Error)
}

class MovieStore {
    
    let imageStore = ImageStore()
    
    let session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
        }()
    
    func processMoviesRequest(data: Data?, error: Error?) -> MoviesResult {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return MovieAPI.movies(fromJSON: jsonData)
    }
    
    private var movies: [Movie] = []
    
    func processImageRequest(data: Data?, error: Error?) -> ImageResult {
        
        guard
            let imageData = data,
            let image = UIImage(data: imageData) else {
                
                // Couldn't create an image
                if data == nil {
                    return .failure(error!)
                }
                else {
                    return .failure(MovieError.imageCreationError)
                }
        }
        
        return .success(image)
    }
    
    func fetchImage(for movie: Movie, detail: Bool, completion: @escaping (ImageResult) -> Void) {
        
        let movieKey = movie.id!
        if let image = imageStore.image(forKey: String(movieKey)), !detail {
            OperationQueue.main.addOperation {
                completion(.success(image))
            }
        }
        let stringURL =  detail ? "https://image.tmdb.org/t/p/original"+movie.backdrop_path! : "https://image.tmdb.org/t/p/original"+movie.poster_path!
        let movieURL = URL(string:stringURL)
        let request = URLRequest(url: movieURL!)
        
        let task = session.dataTask(with: request) {
            (data, response, error) -> Void in
            
            let result = self.processImageRequest(data: data, error: error)
            if(!detail){
                if case let .success(image) = result {
                    self.imageStore.setImage(image, forKey: String(movieKey))
                }
            }
            
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
        task.resume()
    }
    
    func fetchInterestingMovies(completion: @escaping (MoviesResult) -> Void) {
        
        let url = MovieAPI.interestingMoviesURL
        
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            let result = self.processMoviesRequest(data: data, error: error)
            OperationQueue.main.addOperation {
                completion(result)
            }
        })
        task.resume()
    }
    
    func fetchMovieInfo(movieID: Int,completion: @escaping (MovieInfoResult) -> Void) {
        
        let url = URL(string:"https://api.themoviedb.org/3/movie/"+String(movieID)+"?api_key=634b49e294bd1ff87914e7b9d014daed&language=en-US")//MovieAPI.MovieInfoURL
        
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            let result = self.processMovieInfoRequest(data: data, error: error)
            OperationQueue.main.addOperation {
                completion(result)
            }
        })
        task.resume()
    }
    
    
    func processMovieInfoRequest(data: Data?, error: Error?) -> MovieInfoResult {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return MovieAPI.movieInfo(fromJSON: jsonData)
    }
    
}
