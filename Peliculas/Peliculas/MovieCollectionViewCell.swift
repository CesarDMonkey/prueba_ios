//
//  MovieViewController.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet var txtTitle: UILabel!
    @IBOutlet var txtFecha: UILabel!
    @IBOutlet var txtCalificacion: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        update(with: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        update(with: nil)
    }
    
    func update(with image: UIImage?) {
        if let imageToDisplay = image {
            spinner.stopAnimating()
            spinner.assignColor(.blue)
            imageView.image = imageToDisplay
            imageView.makeRounded()
        } else {
            spinner.startAnimating()
            imageView.image = nil
        }
    }
}
extension UIImageView {
    
    func makeRounded() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
}

extension UIActivityIndicatorView {
    func assignColor(_ color: UIColor) {
        style = .gray
        self.color = color
    }
}
