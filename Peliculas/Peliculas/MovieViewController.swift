//
//  MovieViewController.swift
//  Peliculas
//
//  Created by Administrador on 3/27/20.
//  Copyright © 2020 Administrador. All rights reserved.
//

import UIKit

class MovieViewController: UIViewController, UICollectionViewDelegate {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var store: MovieStore!
    let movieDataSource = MovieDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = movieDataSource
        collectionView.delegate = self
        store = MovieStore()
        store.fetchInterestingMovies {
            (moviesResult) -> Void in
            
            switch moviesResult {
            case let .success(movies):
                print("Successfully found \(movies.count) movies.")
                self.movieDataSource.movies = movies
            case let .failure(error):
                print("Error fetching recent movies: \(error)")
                self.movieDataSource.movies.removeAll()
            }
            self.collectionView.reloadSections(IndexSet(integer: 0))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        let movie = movieDataSource.movies[indexPath.row]
        
        // Download the image data, which could take some time
        store.fetchImage(for: movie,detail: false, completion: { (result) -> Void in 
                
            // The index path for the movie might have changed between the
            // time the request started and finished, so find the most
            // interesting index path
            
            guard let movieIndex = self.movieDataSource.movies.firstIndex(of: movie),
                case let .success(image) = result else {
                    return
            }
            let movieIndexPath = IndexPath(item: movieIndex, section: 0)
            
            // When the request finishes, only update the cell if it's still visible
            if let cell = self.collectionView.cellForItem(at: movieIndexPath)
                as? MovieCollectionViewCell {
                cell.update(with: image)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showMovie"?:
            if let selectedIndexPath =
                collectionView.indexPathsForSelectedItems?.first {
                
                let movie = movieDataSource.movies[selectedIndexPath.row]
                
                let destinationVC =
                    segue.destination as! MovieInfoViewController
                destinationVC.movie = movie
                destinationVC.store = store
            }
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
}
